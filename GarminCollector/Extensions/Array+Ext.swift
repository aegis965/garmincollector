//
//  Array+Ext.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/13.
//

import Foundation

extension Array {
    var tails: Self {
        count > 2 ? [first!, last!] : self
    }
}
