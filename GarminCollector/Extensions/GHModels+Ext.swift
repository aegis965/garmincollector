//
//  GHModels+Ext.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/13.
//

import Foundation
import companion

extension GHLoggingDataTypes {
    static var allDataTypes: [Self] {
        [.BBI, .heartRate, .respiration, .spO2, .steps, .stress, .zeroCrossing, .accelerometer]
    }
    
    static var defaultDataTypes: [Self] {
        [.BBI]
    }

    var fullName: String {
        switch self {
        case .BBI:
            return "RR-Interval"
        case .heartRate:
            return "Avg. HR"
        case .respiration:
            return "Respiration"
        case .spO2:
            return "SpO2"
        case .steps:
            return "Steps"
        case .stress:
            return "Stress"
        case .zeroCrossing:
            return "Zero Crossing"
        case .accelerometer:
            return "Accelerometer"
        default:
            return "Unknown"
        }
    }

}

extension GHScannedDevice {
    var friendlyString: String {
        "\(friendlyName ?? "") (\(identifier?.uuidString ?? ""))"
    }
}

extension GHDevice {
    var connectionString: String {
        switch status {
        case .connected:
            return "connected"
        default:
            return "not connected"
        }
    }
}

extension GHRealTimeResult {
    var bbiString: String {
        "bbi \(bbi?.stringValue ?? "") at \(timestamp?.description ?? "")"
    }
}

extension GHRequestDataTypes {
    static var allDataTypes: Self {
        [.loggedBBI, .loggedHeartRate, .loggedRespiration, .loggedSPO2, .loggedSteps, .loggedStress, .loggedZeroCrossing, .loggedAccelerometer]
    }
    
    static var defaultDataTypes: Self {
        .loggedBBI
    }
}

extension GHFetchResult {
    var bbiByHour: [HourlyBBIs] {
        guard let validBBIData = loggedBBIData?.filter({ ($0.timestamp != nil) && ($0.bbi != nil) }), !validBBIData.isEmpty else { return [] }

        // get data start time & end time
        let startHour = validBBIData.min()!.timestamp!.nearestHourStart()
        let endHour = validBBIData.max()!.timestamp!.nextHourStart()

        // stride by hour
        let startHours = Array(stride(from: startHour, through: endHour, by: 3600))

        // group to dictionary & sort values
        let groupedByHour = Dictionary(grouping: validBBIData) { bbiData in startHours.last { bbiData.timestamp! >= $0 }! }
            .map { HourlyBBIs(startHour: $0.key, BBIs: $0.value.sorted()) }
            .sorted(by: { $0.startHour < $1.startHour })

        return groupedByHour
    }
    
    func contains(dataTypes: GHRequestDataTypes) -> Bool {
        var contains = false
        
        if dataTypes.contains(.loggedAccelerometer), !(loggedAccelerometerData?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedBBI), !(loggedBBIData?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedSteps), !(loggedStepData?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedStress), !(loggedStressData?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedRespiration), !(loggedRespirationData?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedSPO2), !(loggedSPO2Data?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedHeartRate), !(loggedHeartRateData?.isEmpty ?? true) {
            contains = true
        }
        
        if dataTypes.contains(.loggedZeroCrossing), !(loggedZeroCrossingData?.isEmpty ?? true) {
            contains = true
        }
        
        return contains
    }
    
    func loggedData(for dataTypes: GHRequestDataTypes) -> [GHLoggedData] {
        var loggedData = [GHLoggedData]()
        
        if dataTypes.contains(.loggedAccelerometer), !(loggedAccelerometerData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedAccelerometerData!)
        }
        
        if dataTypes.contains(.loggedBBI), !(loggedBBIData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedBBIData!)
        }
        
        if dataTypes.contains(.loggedSteps), !(loggedStepData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedStepData!)
        }
        
        if dataTypes.contains(.loggedStress), !(loggedStressData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedStressData!)
        }
        
        if dataTypes.contains(.loggedRespiration), !(loggedRespirationData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedRespirationData!)
        }
        
        if dataTypes.contains(.loggedSPO2), !(loggedSPO2Data?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedSPO2Data!)
        }
        
        if dataTypes.contains(.loggedHeartRate), !(loggedHeartRateData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedHeartRateData!)
        }
        
        if dataTypes.contains(.loggedZeroCrossing), !(loggedZeroCrossingData?.isEmpty ?? true) {
            loggedData.append(contentsOf: loggedZeroCrossingData!)
        }
        
        return loggedData
    }
    
    func describe(truncate: Bool = true) -> [String] {
        var dataDescription = [String]()
    
        if let loggedBBIData = self.loggedBBIData, !loggedBBIData.isEmpty {
            let bbiDataString = (truncate ? loggedBBIData.tails : loggedBBIData)
                .compactMap { bbiData in
                    guard let bbi = bbiData.bbi, let timestamp = bbiData.timestamp else { return nil }
                    return "\(bbi.stringValue) at \(timestamp.description)"
                }
                .joined(separator: truncate ? " ... " : ", ")
            
            dataDescription.append("BBI Data: \(bbiDataString)")
        }
        
        if let loggedStepData = self.loggedStepData, !loggedStepData.isEmpty {
            let stepDataString = (truncate ? loggedStepData.tails : loggedStepData)
                .compactMap { stepData in
                    guard let step = stepData.steps, let timestamp = stepData.timestamp else { return nil }
                    return "\(step.stringValue) at \(timestamp.description)"
                }
                .joined(separator: truncate ? " ... " : ", ")
            
            dataDescription.append("Step Data: \(stepDataString)")
        }
        
        return dataDescription
    }
}

extension GHBBIData: Comparable {
    public static func < (lhs: GHBBIData, rhs: GHBBIData) -> Bool {
        if let lhsDate = lhs.timestamp, let rhsDate = rhs.timestamp {
            return lhsDate < rhsDate
        }
        return lhs.timestamp == nil
    }
}
