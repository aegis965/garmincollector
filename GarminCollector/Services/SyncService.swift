//
//  SyncService.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/12.
//

import os.log
import Foundation
import RxSwift
import RxRelay
import RxSwiftExt
import Action
import companion

class SyncService {
    static let shared = SyncService()
    static let syncInterval = 60
    
    func register(_ device: GHRealTimeDevice) {
        self.currentDevice.accept(device)
    }
    
    func start() {
        updateLastSyncTimeAfterFetchSuccess()
        fetchDataAfterSyncSuccess()
        requestSyncWithRegularInterval()
    }
    
    func getSyncResult() -> Observable<Void> {
        syncAction.elements
    }
    
    func getSyncError() -> Observable<Error> {
        syncAction.underlyingError
    }
    
    func getFetchResult() -> Observable<GHFetchResult> {
        fetchAction.elements.mapAt(\.result)
    }
    
    func getFetchError() -> Observable<Error> {
        fetchAction.underlyingError
    }

    private func requestSyncWithRegularInterval() {
        Observable<Int>.interval(.seconds(Self.syncInterval), scheduler: MainScheduler.instance)
            .startWith(0)
            .mapTo(())
            .debug("[Garmin Auto] Sync Triggered")
            .do(onNext: {
                Logger.autoSync.notice("sync triggered")
            })
            .bind(to: syncAction.inputs)
            .disposed(by: disposeBag)
    }
    
    private func fetchDataAfterSyncSuccess() {
        syncAction.elements
            .map { _ in
                var lastSyncTime = PreferenceManager.shared.getCurrentLastSyncTime()
                if lastSyncTime == 0.0 {
                    lastSyncTime = Date().addingTimeInterval(-600).timeIntervalSince1970
                }
                return (Date(timeIntervalSince1970: lastSyncTime), Date())
            }
            .delay(.seconds(5), scheduler: MainScheduler.instance)
            .debug("[Garmin Auto] Fetch Triggered")
            .do(onNext: {
                Logger.autoFetch.notice("fetch range from \($0.fromDate.description) to \($0.toDate.description)")
            })
            .bind(to: fetchAction.inputs)
            .disposed(by: disposeBag)
    }
    
    private func updateLastSyncTimeAfterFetchSuccess() {
        fetchAction.elements
            .mapAt(\.lastSyncTime)
            .subscribe(onNext: {
                PreferenceManager.shared.setLastSyncTime(to: $0.timeIntervalSince1970)
            })
            .disposed(by: disposeBag)
    }
    
    private func performSync() -> Observable<Void> {
        currentDevice
            .compactMap { $0 }
            .take(1)
            .flatMapLatest { device in
                GarminManager.shared.requestSync(for: device)
                    .andThen(GarminManager.shared.getSyncResult(for: device.identifier))
            }
            .mapAt(\.1)
            .compactMap { (result: GHSyncResult?) in
                if case .failure(let error) = result { throw error }
                return result
            }
            .retry(.exponentialDelayed(maxCount: 5, initial: 2.0, multiplier: 1.5))
            .take(until: { (result: GHSyncResult) in
                switch result {
                case .completed:
                    return true
                default:
                    return false
                }
            })
            .debug("[Garmin Auto] Sync")
            .toArray()
            .mapTo(())
            .do(onSuccess: {
                Logger.autoSync.notice("sync completed")
            }, onError: { error in
                Logger.autoSync.error("sync failed, error: \(error.localizedDescription)")
            })
            .asObservable()
    }
    
    private func performFetch(from fromDate: Date, to toDate: Date) -> Observable<(lastSyncTime: Date, result: GHFetchResult)> {
        currentDevice
            .compactMap { $0 }
            .take(1)
            .flatMapLatest { device in
                GarminManager.shared.fetchData(for: device, dataTypes: .defaultDataTypes, from: fromDate, to: toDate)
            }
            .debug("[Garmin Auto] Fetch")
            .do(onNext: {
                Logger.autoFetch.notice("data fetched from \(fromDate.description) to \(toDate.description): \($0.describe(truncate: false))")
            }, onError: { error in
                Logger.autoFetch.error("fetch data failed, error: \(error.localizedDescription)")
            })
            .retry(.exponentialDelayed(maxCount: 5, initial: 2.0, multiplier: 1.5))
            .map { result in
                let lastSyncTime = result.loggedData(for: .defaultDataTypes).compactMap { $0.loggedTimestamp }.max() ?? toDate
                return (lastSyncTime, result)
            }
    }
    
    private let disposeBag = DisposeBag()
    
    private let currentDevice = ReplayRelay<GHRealTimeDevice>.create(bufferSize: 1)
    
    private lazy var syncAction = Action<Void, Void> { [weak self] in
        guard let self = self else { return .empty() }
        
        return self.performSync()
            .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
    }
    
    private lazy var fetchAction = Action<(fromDate: Date, toDate: Date), (lastSyncTime: Date, result: GHFetchResult)> { [weak self] request in
        guard let self = self else { return .empty() }

        return self.performFetch(from: request.fromDate, to: request.toDate)
            .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
    }
}
