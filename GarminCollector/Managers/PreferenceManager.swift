//
//  PreferenceManager.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/2/26.
//

import Foundation
import RxSwift
import RxCocoa

struct Preference {
    static let deviceId = "deviceId"
    static let lastSyncTime = "lastSyncTime"
}

final class PreferenceManager {
    static let shared = PreferenceManager()

    func setDeviceId(to deviceId: UUID) {
        UserDefaults.standard.set(deviceId.uuidString, forKey: Preference.deviceId)
    }
    
    func getCurrentDeviceId() -> UUID? {
        if let deviceIdString = UserDefaults.standard.string(forKey: Preference.deviceId),
           let deviceId = UUID(uuidString: deviceIdString) {
            return deviceId
        }
        return nil
    }
    
    func getDeviceId() -> Observable<UUID> {
        UserDefaults.standard.rx.observe(String.self, Preference.deviceId)
            .compactMap { deviceIdString in
                if let deviceIdString = deviceIdString,
                   let deviceId = UUID(uuidString: deviceIdString) {
                    return deviceId
                }
                return nil
            }
    }
    
    func setLastSyncTime(to lastSyncTime: TimeInterval) {
        UserDefaults.standard.set(lastSyncTime, forKey: Preference.lastSyncTime)
    }
    
    func getCurrentLastSyncTime() -> TimeInterval {
        UserDefaults.standard.double(forKey: Preference.lastSyncTime)
    }
    
    func getLastSyncTime() -> Observable<TimeInterval> {
        UserDefaults.standard.rx.observe(TimeInterval.self, Preference.lastSyncTime)
            .compactMap { $0 }
    }
}
