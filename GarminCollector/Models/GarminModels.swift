//
//  GarminModels.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/2/26.
//

import Foundation
import companion

enum GHSyncResult {
    case started
    case inProgress(Double)
    case completed
    case failure(Error)
}

typealias GHLoggingState = (enabled: Bool, interval: Int?)

protocol GHLoggedData {
    var loggedTimestamp: Date? { get }
}

extension GHBBIData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHHeartRateData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHRespirationData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHSPO2Data: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHStepData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHStressData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHZeroCrossingData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

extension GHAccelerometerData: GHLoggedData {
    var loggedTimestamp: Date? { timestamp }
}

