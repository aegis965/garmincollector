//
//  ContentView.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/2/25.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel: ContentViewModel

    var body: some View {
        List {
            Group {
                ExpandableSection(header: Text("Scan"), content: scanView)
                ExpandableSection(header: Text("Select Scanned Devices"), content: scannedDevicesView)
                ExpandableSection(header: Text("Pair"), content: pairView)
                ExpandableSection(header: Text("Connection"), content: connectionView)
                ExpandableSection(header: Text("Auto"), content: autoSyncAndFetchView)
                ExpandableSection(header: Text("Data Logging"), content: loggingView)
                ExpandableSection(header: Text("Data Streaming"), content: streamingView)
                ExpandableSection(header: Text("Sync"), content: syncView)
                ExpandableSection(header: Text("Fetch Data"), content: fetchView)
                ExpandableSection(header: Text("BBI Stats"), content: BBIStatsView)
            }
            Group {
                ExpandableSection(header: Text("Misc"), content: miscView)
            }
        }
    }
    
    @ViewBuilder func scanView() -> some View {
        VStack(spacing: 20) {
            HStack {
                Spacer()
                Button("Start Scan", action: viewModel.scan)
                    .buttonStyle(BorderedButtonStyle())
                Spacer()
                Button("Stop Scan", action: viewModel.stopScan)
                    .buttonStyle(BorderedButtonStyle())
                Spacer()
            }
            LogStack(entries: $viewModel.scanLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func scannedDevicesView() -> some View {
        List(viewModel.scannedDevices, id: \.identifier) { device in
            Button(device.friendlyString) {
                self.viewModel.deviceSelected(device)
            }
        }
        .frame(height: 100)
        .padding(.vertical)
    }
    
    @ViewBuilder func pairView() -> some View {
        VStack(spacing: 20) {
            Text(viewModel.deviceScanned ? "Device to Pair: \(viewModel.deviceToPair!.friendlyString)" : "")
            HStack {
                Spacer()
                Button("Pair", action: viewModel.pair)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.deviceScanned || viewModel.devicePaired)
                Spacer()
                Button("Unapir", action: viewModel.unpair)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
            }
            LogStack(entries: $viewModel.pairLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func connectionView() -> some View {
        VStack(spacing: 20) {
            Text(viewModel.devicePaired ? "Paired Device: \(viewModel.pairedDevice!.friendlyString)" : "")
            Text(viewModel.devicePaired ? "Device is \(viewModel.pairedDevice!.connectionString)" : "")
            LogStack(entries: $viewModel.connectionLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func autoSyncAndFetchView() -> some View {
        VStack(spacing: 20) {
            Text("Auto Sync and Fetch is \(viewModel.autoSyncAndFetchStarted ? "on" : "off")")
            Text("Last Sync: \(viewModel.lastAutoSyncTime)")
            Button("Start Auto Sync And Fetch", action: viewModel.startAutoSyncAndFetch)
                .buttonStyle(BorderedButtonStyle())
                .disabled(!viewModel.devicePaired || viewModel.autoSyncAndFetchStarted)
            LogStack(entries: $viewModel.autoSyncAndFetchLogs)
        }
    }
    
    @ViewBuilder func loggingView() -> some View {
        VStack(spacing: 20) {
            Text("Logging is \(viewModel.logging ? "on" : "off")")
            HStack {
                Spacer()
                Button("Start", action: viewModel.startLogging)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
                Button("Stop", action: viewModel.stopLogging)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
                Button("Check", action: viewModel.checkLogging)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
            }
            LogStack(entries: $viewModel.loggingLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func streamingView() -> some View {
        VStack(spacing: 20) {
            HStack {
                Spacer()
                Button("Start Streaming", action: viewModel.startStreaming)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
                Button("Stop Streaming", action: viewModel.stopStreaming)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
            }
            LogStack(entries: $viewModel.streamingLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func syncView() -> some View {
        VStack(spacing: 20) {
            Button("Sync", action: viewModel.sync)
                .buttonStyle(BorderedButtonStyle())
                .disabled(!viewModel.devicePaired)
            LogStack(entries: $viewModel.syncLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func fetchView() -> some View {
        VStack(spacing: 20) {
            DatePicker("From", selection: $viewModel.fetchFrom)
            DatePicker("To", selection: $viewModel.fetchTo)
            Button("Fetch", action: viewModel.fetch)
                .buttonStyle(BorderedButtonStyle())
                .disabled(!viewModel.devicePaired)
            LogStack(entries: $viewModel.fetchLogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func BBIStatsView() -> some View {
        VStack(spacing: 20) {
            Text(viewModel.totalBBILogs)
            LogStack(entries: $viewModel.hourlyBBILogs)
        }
        .padding(.vertical)
    }
    
    @ViewBuilder func miscView() -> some View {
        VStack(spacing: 20) {
            HStack {
                Spacer()
                Button("Sync And Check Logging", action: self.viewModel.syncAndCheckLogging)
                    .buttonStyle(BorderedButtonStyle())
                    .disabled(!viewModel.devicePaired)
                Spacer()
            }
        }
        .padding(.vertical)
    }
}

struct LogStack: View {
    @Binding var entries: [String]
    
    private let colors: [Color] = [.yellow, .blue, .green, .orange, .pink, .purple]
    
    private var bottom: Int {
        entries.count - 1
    }

    var body: some View {
        ScrollView {
            ScrollViewReader { proxy in
                ForEach(0..<entries.count, id: \.self) {
                    Text(entries[$0])
                        .foregroundColor(colors[$0 % colors.count])
                }
                .onAppear {
                    proxy.scrollTo(bottom, anchor: .center)
                }
                .onChange(of: entries.count) { _ in
                    proxy.scrollTo(bottom - 1)
                }
            }
            .frame(maxWidth: .infinity, maxHeight: 100)
        }
    }
}

struct ExpandableSection<Content: View>: View {
    @State private var isExpanded = false

    let header: Text
    let content: () -> Content

    var body: some View {
        Section(header: headerView()) {
            if isExpanded {
                content()
            }
        }
    }
    
    @ViewBuilder private func headerView() -> some View {
        HStack {
            header
                .lineLimit(1)
                .fixedSize(horizontal: true, vertical: false)
            ZStack {
                Color.black.opacity(0.001)
                Spacer()
            }
        }
        .onTapGesture {
            withAnimation {
                isExpanded.toggle()
            }
        }
    }
}

struct BorderedButtonStyle: ButtonStyle {
    var color = Color.blue

    func makeBody(configuration: Configuration) -> some View {
        BorderedButton(color: color, configuration: configuration)
    }
    
    struct BorderedButton: View {
        @Environment(\.isEnabled) private var isEnabled: Bool
        
        let color: Color
        let configuration: ButtonStyle.Configuration
        
        var fgColor: Color {
            isEnabled ? color : color.opacity(0.25)
        }
        
        var body: some View {
            configuration.label
                .foregroundColor(fgColor)
                .padding(6)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(fgColor, lineWidth: 2)
                )
                .opacity(configuration.isPressed ? 0.5 : 1)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: ContentViewModel())
    }
}
