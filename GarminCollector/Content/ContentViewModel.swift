//
//  ContentViewModel.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/2/25.
//

import os.log
import Combine
import RxSwift
import RxRelay
import companion

final class ContentViewModel: ObservableObject {
    // Scanning
    @Published var scanning = false
    @Published var scannedDevices = [GHScannedDevice]()
    @Published var scanLogs = [String]()
    
    // Pairing
    @Published var deviceToPair: GHScannedDevice?
    @Published var pairedDevice: GHRealTimeDevice?
    @Published var pairLogs = [String]()
    var deviceScanned: Bool { deviceToPair != nil }
    var devicePaired: Bool { pairedDevice != nil }
    
    // Connection
    @Published var connectionLogs = [String]()
    
    // Auto Sync
    @Published var autoSyncAndFetchStarted = false
    @Published var lastAutoSyncTime = ""
    @Published var autoSyncAndFetchLogs = [String]()

    // Logging
    @Published var logging = false
    @Published var loggingLogs = [String]()
    
    // Streaming
    @Published var streaming = false
    @Published var streamingLogs = [String]()
    
    // Syncing
    @Published var syncing = false
    @Published var syncLogs = [String]()
    
    // Fetching
    @Published var fetching = false
    @Published var fetchFrom = Date().addingTimeInterval(-86400)
    @Published var fetchTo = Date()
    @Published var fetchLogs = [String]()
    
    // Data
    private var bbiByHour = [HourlyBBIs]() {
        didSet {
            guard !bbiByHour.isEmpty else { return }

            let startHour = bbiByHour.min()!.startHour
            let hourlyBBIStats = bbiByHour.map { $0.stats }

            hourlyBBILogs = hourlyBBIStats.map { $0.statsString }
            totalBBILogs = hourlyBBIStats.reduce(BBIStats(startTime: startHour, endTime: startHour.nextHourStart(), total: 0, missing: 0, samples: 0), +).statsString
        }
    }
    @Published var hourlyBBILogs = [String]()
    @Published var totalBBILogs = ""

    // View Actions
    func deviceSelected(_ device: GHScannedDevice) {
        self.deviceToPair = device
    }

    func scan() {
        scannedDevices = []
        GarminManager.shared.scan()
        scanning = true
        scanLogs.append("started scanning")
    }
    
    func stopScan() {
        GarminManager.shared.stopScan()
        scanning = false
        scanLogs.append("stopped scanning")
    }
    
    func pair() {
        guard let deviceToPair = deviceToPair else { return }
        GarminManager.shared.pair(with: deviceToPair)
        pairLogs.append("started pairing with \(deviceToPair.friendlyString)")
    }
    
    func unpair() {
        guard let pairedDevice = pairedDevice else { return }
        GarminManager.shared.unpair(with: pairedDevice)
        self.pairedDevice = nil
        pairLogs.append("unpaired \(pairedDevice.friendlyString)")
    }
    
    func startAutoSyncAndFetch() {
        guard pairedDevice != nil else { return }

        startLogging()
        SyncService.shared.start()
        self.autoSyncAndFetchStarted = true
    }
    
    func startLogging() {
        guard let pairedDevice = pairedDevice else { return }
        
        // Test Set Logging State
        Observable.from(GHLoggingDataTypes.defaultDataTypes)
            .concatMap { (dataTypes: GHLoggingDataTypes) -> Completable in
                GarminManager.shared.startLogging(for: pairedDevice, dataTypes: dataTypes)
                    .debug("[Start Garmin Logging]")
                    .do(onError: { [weak self] error in
                        self?.loggingLogs.append("start logging \(dataTypes.fullName) errored: \(error.localizedDescription)")
                    })
                    .catch { _ in .empty() }
            }
            .observe(on: MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.loggingStateUpdateTrigger.accept(())
            })
            .disposed(by: disposeBag)
    }
    
    func stopLogging() {
        guard let pairedDevice = pairedDevice else { return }
        
        // Test Set Logging State
        Observable.from(GHLoggingDataTypes.defaultDataTypes)
            .concatMap { (dataTypes: GHLoggingDataTypes) -> Completable in
                GarminManager.shared.stopLogging(for: pairedDevice, dataTypes: dataTypes)
                    .debug("[Stop Garmin Logging]")
                    .do(onError: { [weak self] error in
                        self?.loggingLogs.append("stop logging \(dataTypes.fullName) errored: \(error.localizedDescription)")
                    })
                    .catch { _ in .empty() }
            }
            .observe(on: MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.loggingStateUpdateTrigger.accept(())
            })
            .disposed(by: disposeBag)
    }
    
    func startStreaming() {
        guard let pairedDevice = pairedDevice else { return }
        GarminManager.shared.startStreaming(for: pairedDevice, dataTypes: .beatToBeatInterval)
        streaming = true
        streamingLogs.append("streaming requested")
    }
    
    func stopStreaming() {
        guard let pairedDevice = pairedDevice else { return }
        GarminManager.shared.stopStreaming(for: pairedDevice, dataTypes: .beatToBeatInterval)
        streaming = false
        streamingLogs.append("streaming termination requested")
    }
    
    func sync() {
        guard let pairedDevice = pairedDevice else { return }
        
        syncLogs.append("requesting sync")
        
        GarminManager.shared.requestSync(for: pairedDevice)
            .observe(on: MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.syncing = true
                self?.syncLogs.append("sync requested")
            }, onError: { [weak self] error in
                self?.syncing = false
                self?.syncLogs.append("request sync failed, error: \(error.localizedDescription)")
            })
            .disposed(by: disposeBag)
    }
    
    func fetch() {
        guard let pairedDevice = pairedDevice else { return }

        fetching = true
        fetchLogs.append("fetching data from \(fetchFrom.description) to \(fetchTo.description)")

        GarminManager.shared.fetchData(for: pairedDevice, dataTypes: .defaultDataTypes, from: fetchFrom, to: fetchTo)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] result in
                let bbiByHour = result.bbiByHour
                print("bbi by hour: \(bbiByHour.map { $0.BBIs.compactMap { $0.bbi }.count })")
                self?.bbiByHour.merge(result.bbiByHour)
                self?.fetchLogs.append("fetch data successed")
                self?.fetching = false
            }, onFailure: { [weak self] error in
                self?.fetchLogs.append("fetch data failed, error: \(error.localizedDescription)")
                self?.fetching = false
            })
            .disposed(by: disposeBag)
    }
    
    func checkLogging() {
        loggingStateUpdateTrigger.accept(())
    }
    
    func syncAndCheckLogging() {
        guard let pairedDevice = pairedDevice else { return }
        
        GarminManager.shared.requestSync(for: pairedDevice)
            .observe(on: MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.loggingStateUpdateTrigger.accept(())
                self?.syncing = true
                self?.syncLogs.append("sync requested")
            }, onError: { [weak self] error in
                self?.syncing = false
                self?.syncLogs.append("request sync failed, error: \(error.localizedDescription)")
            })
            .disposed(by: disposeBag)
    }
    
    init() {
        bindScanResult()
        bindPairingResult()
        bindRealTimeDevice()
        bindLoggingState()
        bindSyncResult()
        bindLastSyncTime()
        bindLogs()
    }
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    
    private lazy var loggingStateUpdateTrigger: PublishRelay<Void> = {
        let loggingStateUpdateTrigger = PublishRelay<Void>()
        
        Observable<Int>.interval(.seconds(30), scheduler: MainScheduler.instance)
            .mapTo(())
            .bind(to: loggingStateUpdateTrigger)
            .disposed(by: disposeBag)
        
        return loggingStateUpdateTrigger
    }()
    
    private func bindScanResult() {
        GarminManager.shared.getScanResult()
            .compactMap { scanResult -> GHScannedDevice? in
                switch scanResult {
                case .success(let device):
                    return device
                case .failure:
                    return nil
                }
            }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.scannedDevices.append(device)
            })
            .disposed(by: disposeBag)
    }
    
    private func bindPairingResult() {
        GarminManager.shared.getPairingResult()
            .compactMap { [weak self] (deviceId, result) -> GHRealTimeDevice? in
                guard deviceId == self?.deviceToPair?.identifier, case .success(let device) = result else { return nil }
                return device
            }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                PreferenceManager.shared.setDeviceId(to: device.identifier)
                self?.pairedDevice = device
            })
            .disposed(by: disposeBag)
    }
    
    private func bindRealTimeDevice() {
        PreferenceManager.shared.getDeviceId()
            .distinctUntilChanged()
            .flatMapLatest { deviceId in GarminManager.shared.getRealTimeDevice().filter { $0.identifier == deviceId } }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.pairedDevice = device
                self?.loggingStateUpdateTrigger.accept(())
                SyncService.shared.register(device)
            })
            .disposed(by: disposeBag)
    }
    
    private func bindLoggingState() {
        loggingStateUpdateTrigger
            .withUnretained(self)
            .compactMap { (viewModel, _) in viewModel.pairedDevice }
            .flatMapFirst { device -> Single<[GHLoggingState]> in
                // Test Get Logging State
                Observable.from([GHLoggingDataTypes]([.BBI, .heartRate, .respiration, .spO2, .steps, .stress, .zeroCrossing, .accelerometer]))
                    .concatMap { (dataTypes: GHLoggingDataTypes) in
                        GarminManager.shared.getLoggingState(for: device, dataTypes: dataTypes)
                            .debug("[Get Garmin Logging State]")
                            .do(onSuccess: { [weak self] state in
                                let log = "\(dataTypes.fullName) logging is \(state.enabled ? "on" : "off") with interval \(state.interval ?? 0)"
                                print(log)
                                self?.loggingLogs.append(log)
                            }, onError: { [weak self] error in
                                let log = "update logging state \(dataTypes.fullName) errored: \(error.localizedDescription)"
                                print(log)
                                self?.loggingLogs.append(log)
                            })
                            .catchAndReturn((false, nil))
                    }
                    .toArray()
            }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] loggingStates in
                if let bbiLogging = loggingStates.first {
                    self?.logging = bbiLogging.enabled
                    self?.loggingLogs.append(bbiLogging.enabled ? "started logging BBI" : "stopped logging BBI")
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func bindSyncResult() {
        PreferenceManager.shared.getDeviceId()
            .distinctUntilChanged()
            .flatMapLatest { GarminManager.shared.getSyncResult(for: $0) }
            .map { $0.1 }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] syncResult in
                switch syncResult {
                case .started, .inProgress:
                    self?.syncing = true
                case .completed, .failure:
                    self?.syncing = false
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func bindLastSyncTime() {
        PreferenceManager.shared.getLastSyncTime()
            .map { Date(timeIntervalSince1970: $0).description }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.lastAutoSyncTime = $0
            })
            .disposed(by: disposeBag)
    }
    
    private func bindLogs() {
        GarminManager.shared.getScanResult()
            .map { result -> String in
                switch result {
                case .success(let device):
                    return "\(device.friendlyString) scanned"
                case .failure(let error):
                    return "scan failed, error: \(error.localizedDescription)"
                }
            }
            .debug("[Garmin Scan]")
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] scanResult in
                Logger.scan.notice("\(scanResult)")
                self?.scanLogs.append(scanResult)
            })
            .disposed(by: disposeBag)

        GarminManager.shared.getPairingResult()
            .map { (deviceId, result) -> String in
                switch result {
                case .success(let device):
                    return "\(device.friendlyString) is paired"
                case .failure(let error):
                    return "\(deviceId) pairing failed, error: \(error.localizedDescription)"
                }
            }
            .debug("[Garmin Pairing]")
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] pairingResult in
                Logger.pair.notice("\(pairingResult)")
                self?.pairLogs.append(pairingResult)
            })
            .disposed(by: disposeBag)
    
        PreferenceManager.shared.getDeviceId()
            .distinctUntilChanged()
            .flatMapLatest { deviceId in GarminManager.shared.getRealTimeDevice().filter { $0.identifier == deviceId } }
            .map { device -> String in
                let connectionStatus = device.status == .connected ? "connected" : "not connected"
                return "\(device.friendlyString) is \(connectionStatus)"
            }
            .debug("[Garmin Connection]")
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] connectionResult in
                Logger.connection.notice("\(connectionResult)")
                self?.connectionLogs.append(connectionResult)
            })
            .disposed(by: disposeBag)

        PreferenceManager.shared.getDeviceId()
            .distinctUntilChanged()
            .flatMapLatest { GarminManager.shared.getRealTimeResult(for: $0) }
            .map { (_, result) -> String in
                switch result {
                case .success(let realTimeResult):
                    return realTimeResult.bbiString
                case .failure(let error):
                    return "Failed to receive real time data, error: \(error.localizedDescription)"
                }
            }
            .debug("[Garmin Streaming]")
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] realTimeResult in
                Logger.streaming.notice("\(realTimeResult)")
                self?.streamingLogs.append(realTimeResult)
            })
            .disposed(by: disposeBag)
        
            
        PreferenceManager.shared.getDeviceId()
            .distinctUntilChanged()
            .flatMapLatest { GarminManager.shared.getSyncResult(for: $0) }
            .map { (_, result) -> String in
                switch result {
                case .started:
                    return "sync started"
                case .inProgress(let progress):
                    return "sync in progress: \(progress)"
                case .completed:
                    return "sync completed"
                case .failure(let error):
                    return "sync failed, error: \(error.localizedDescription)"
                }
            }
            .debug("[Garmin Sync]")
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] syncResult in
                Logger.sync.notice("\(syncResult)")
                self?.syncLogs.append(syncResult)
            })
            .disposed(by: disposeBag)
        
        SyncService.shared.getSyncResult()
            .mapTo("sync completed")
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] syncResult in
                self?.autoSyncAndFetchLogs.append(syncResult)
            })
            .disposed(by: disposeBag)
        
        SyncService.shared.getSyncError()
            .map { error in "sync failed, error: \(error.localizedDescription)" }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] syncResult in
                self?.autoSyncAndFetchLogs.append(syncResult)
            })
            .disposed(by: disposeBag)
        
        SyncService.shared.getFetchResult()
            .map { $0.describe(truncate: false) }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] fetchResult in
                self?.autoSyncAndFetchLogs.append(contentsOf: fetchResult)
            })
            .disposed(by: disposeBag)
        
        SyncService.shared.getFetchError()
            .map { error in "fetch data failed, error: \(error.localizedDescription)" }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] fetchResult in
                self?.autoSyncAndFetchLogs.append(fetchResult)
            })
            .disposed(by: disposeBag)
    }
}

